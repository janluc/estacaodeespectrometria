/***********************************************************************************************
* MODULE:     user_settings.c
* PURPOSE:    user setting loading and storing
* WRITTEN BY: NDA
* COPYRIGHT:  CAEN S.p.A all rights reserved
* USAGE:      
*             compiled with gcc ?!?!?
*             platform Linux
************************************************************************************************/
////////////////////////////////////////////
// File includes
////////////////////////////////////////////
#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include "common_defs.h"
#include "user_settings.h"
#include "../N957/N957Lib.h"

////////////////////////////////////////////
// File local defines
////////////////////////////////////////////
//JULIO#define DEF_LOG_FILENAME					"data.log"					/* read board values default filename */

#define DEF_CONFIG_FILENAME					"N957Run.conf"				/* configuration default filename */

//JULIO#define DEF_GNU_PLOT_PATH					"../gnuplot/"				/* GNUPLOT default path */

#define DEF_MAX_NUM_SAMPLES					-1							/* default number of samples */

#define DEF_BLDIM							32768						/* default value for bldim */

#define DEF_MODE							N957ControlModeAuto			/* default value for acquisition mode */

//JULIO#define DEF_GNU_PLOT_REFRESH				500							/* default gnu plot refresh rate (msec) */

//JULIO#define DEF_GNU_PLOT_X_SCALE				1.0							/* default gnu plot X conversion factor */


////////////////////////////////////////////
// File local variables declaration
////////////////////////////////////////////

////////////////////////////////////////////
// File local methods declaration
////////////////////////////////////////////
//static BOOL parse_config_file( user_setting_data* p_data);
static char* trim( char charset[], int n_chars, const char* string);

////////////////////////////////////////////
// Global visible variables declaration
////////////////////////////////////////////

/**************************************************
**************************************************/
BOOL user_settings_open( user_setting_data* p_data)
{
	// module variable init
	p_data->m_log_filename= (char*)malloc( MAX_FILENAME_LENGHT);
	p_data->m_config_filename= (char*)malloc( MAX_FILENAME_LENGHT);

	if( p_data->m_config_filename== NULL)
		TRACE("ME12:user setting module: insufficient memory !\n");
	else
		strcpy( p_data->m_config_filename, DEF_CONFIG_FILENAME);

	p_data->m_max_num_samples= DEF_MAX_NUM_SAMPLES;
	p_data->m_debug= N957_FALSE;
	p_data->m_bldim= DEF_BLDIM;
	p_data->m_mode= DEF_MODE;
	p_data->m_N957_handle= N957_INVALID_HANDLE_VALUE;
	p_data->m_log_to_file= N957_FALSE;
	//p_data->m_bd_num;


	return TRUE;
}

/**************************************************
**************************************************/
BOOL user_settings_close( user_setting_data* p_data) {
	//
	// Release board resources
	if( p_data->m_N957_handle!= N957_INVALID_HANDLE_VALUE) {
		N957_End( p_data->m_N957_handle);
	}

	// module variable free
	if( p_data->m_log_filename)
	 	free( p_data->m_log_filename);
	if( p_data->m_config_filename)
		free( p_data->m_config_filename);
	// setting invalid values ...
	p_data->m_log_filename= NULL;
	p_data->m_config_filename= NULL;
	// JULIO p_data->m_gnu_plot_path= NULL;

	return TRUE;
}

/**************************************************
**************************************************/
/*BOOL user_settings_parse_input_param( user_setting_data* p_data, int argc, char* argv[]) {
	//BOOL help_to_show= FALSE;
	int i;
	/*for( i= 1; i< argc; i++)
	{
		if( !strcmp( argv[i], "-h")||
			!strcmp( argv[i], "-H"))
		{
			//JULIOhelp_to_show= TRUE;
		} 
		else if( !strncmp( argv[i], "-f", 2)||
			!strncmp( argv[i], "-F", 2))
		{
			// config filename
			strcpy( p_data->m_config_filename, argv[i]+ 2);
		}
		/*JULIOelse
		{
			// Unknown parameter
			TRACE1("\nuser settings: ignored command line parameter '%s'", argv[i]);
			TRACE("\n  restart with -h flag for help on usage\n");
			help_to_show= TRUE;

		}
	}
	if( !parse_config_file( p_data))
		return FALSE;
	return TRUE;
}*/
/**************************************************
**************************************************/
BOOL parse_config_file( user_setting_data* p_data) {
	//char tmp_filename[ MAX_FILENAME_LENGHT];
	char line[ 200];
	char *str;
	FILE *p_conf_file= NULL;
	BOOL ret_val= TRUE;
	N957ErrorCodes N957_error_code;					// returned error code
	char trim_space_charset[]= {' ', '\t'};
	//char trim_text_charset[]= {' ', '\t', '"'};


	if( ( p_conf_file= fopen( p_data->m_config_filename, "r"))== NULL){
	    TRACE1("Can't open configuration file %s\n", p_data->m_config_filename);
	    return FALSE;
	}
	if( (N957_Init( (short)p_data->m_bd_num, &p_data->m_N957_handle)))
	{	
		TRACE("hi");
		ret_val= FALSE;
		goto exit_point;
	}
	
	while(!feof( p_conf_file)) {
		fscanf( p_conf_file, "%s", line);  // read one string from the file
		str= trim( trim_space_charset, sizeof( trim_space_charset), line);
		if ( str[0] == '#') {
			fgets( line, sizeof( line), p_conf_file);  // skip the whole line (comment)
			continue;
		} 

		if (strstr(str, "MAX_NUM_SAMPLES")!=NULL) {
			fscanf(p_conf_file, "%ld", &p_data->m_max_num_samples);
			continue;
		}
		// Data Block dim
		if (strstr(str, "DATA_BLOCK_DIM")!=NULL) {
			long tmp= DEF_BLDIM;
			fscanf(p_conf_file, "%ld", &tmp);
			if(( tmp> 65536)|| (tmp<= 0))
			{
				TRACE1( "\nuser settings: block dim samples out of range [1..65536]: defaulting to %i", DEF_BLDIM);
				tmp= DEF_BLDIM;
			}
			p_data->m_bldim= ( N957_UINT16)tmp;
			continue;
		}
		// enable debugging
		if (strstr(str, "DEBUG")!=NULL) {
			int tmp= 0;
			fscanf(p_conf_file, "%d", &tmp);
			p_data->m_debug= tmp!= 0;
			continue;
		}
		// Acquisition mode
		if (strstr(str, "ACQ_MODE")!=NULL) {
			int tmp= DEF_MODE;
			fscanf(p_conf_file, "%d", &tmp);
			switch( tmp)
			{
			case N957ControlModeExtGate:
			case N957ControlModeAuto:
				break;
			default:
				TRACE2( "\nuser settings: Unhandled Mode %lu: defaulting to %i", (long)tmp, DEF_MODE);
				tmp= DEF_MODE;
				break;
			}
			p_data->m_mode= ( N957ControlModes) tmp;
			continue;
		}
 
		// Generic Write
		if (strstr(str, "WRITE_REGISTER")!=NULL) {
			int addr, data;
			fscanf(p_conf_file, "%x", &addr);
			fscanf(p_conf_file, "%x", &data);
			if ( N957_WriteReg( p_data->m_N957_handle, ( N957_UINT16)addr, ( N957_UINT16)data)!= N957Success) {
				TRACE1("Write failure at address %08lX\n", (long)addr);
				goto exit_point;
			}                
		}
		//Set LLD Threshold
		if (strstr(str, "LLD_THR_VAL")!=NULL) {
			fscanf(p_conf_file, "%c", &p_data->m_lld);
			if( ( N957_error_code= N957_SetLLD( p_data->m_N957_handle, p_data->m_lld))!=N957Success){
				TRACE( N957_DecodeError( N957_error_code));
				ret_val= FALSE;
				goto exit_point;
			}
			continue;
		}

	}
exit_point:
	fclose( p_conf_file);
	return ret_val;
}

char* trim( char charset[], int n_chars, const char* string){
	int i;
	char* str= (char* )string;
	BOOL found_char;
	// Trim leading chars
	found_char= TRUE;
	while( found_char&& strlen( str) ) {
		found_char= FALSE;
		for( i= 0; i< n_chars; i++) {
			if( str[0]== charset[ i]){
				found_char= TRUE;
				++str;
				break;
			}
		}
	}
	// Trim trailing chars
	found_char= TRUE;
	while( found_char&& strlen( str) ) {
		found_char= FALSE;
		for( i= 0; i< n_chars; i++) {
			if( str[ strlen( str)- 1]== charset[ i]){
				found_char= TRUE;
				str[ strlen( str)- 1]= '\0';
				break;
			}
		}
	}
	return str;
}
