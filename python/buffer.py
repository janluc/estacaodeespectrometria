#!/usr/bin/env python

import matplotlib
matplotlib.use('TkAgg')
import os
import sys
import pexpect
import numpy as np
import Tkinter as tk
import tkMessageBox
import tkFileDialog
import matplotlib.pyplot as plt
import time
from pylab import *
from os.path import basename
#from clsbeep import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure

from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
# implement the default mpl key bindings
from matplotlib.figure import Figure


import sys
if sys.version_info[0] < 3:
    import Tkinter as Tk
else:
    import tkinter as Tk



Plot=dict( Data=dict(), Plotbuttons=list(), Pcontrol=list(), Name=list() )
Interface = dict(frames=list(), buttons=list(), labels=list(), spinboxes=list(), entries=list(), labelframes=list())

event=dict(last_x=int ,last_y=int)
j=0 #numero de botoes (facilita)
n=0;#numero de botoes removidos (facilita)




Axis=dict( x_max=int , x_min=int, y_max=int, y_min=int, x_click=float, y_click=float)

root = Tk.Tk()
root.minsize(825,640)
root.wm_title("Buffer")

def _quit():
    root.quit()     # stops mainloop
    root.destroy()  # this is necessary on Windows to prevent
                    # Fatal Python Error: PyEval_RestoreThread: NULL tstate







f = plt.Figure(figsize=(5,4), dpi=100)
a = f.add_subplot(111)
a.set_xlabel('Channels')
a.set_ylabel('Counts')
f.suptitle('Buffer')


########################################################################
########################################################################
def refresh():
    global  Plot
    a.cla()
    for i in range(len(Plot['Plotbuttons'])):
        if Plot['Pcontrol'][i]:            
            a.plot(Plot['Data'][i][:,0],Plot['Data'][i][:,1], label=Plot['Name'][i])   #Plot['Data'][i][0],Plot['Data'][i][1])        
    a.autoscale_view()
    a.legend()  
    f.canvas.draw()       

def plot_select(name_plot, button):
    global Plot
    j=Plot['Name'].index(name_plot);
    if Plot['Pcontrol'][j]:
        button[j]['relief']='raised'
        Plot['Pcontrol'][j] = 0
    else:
        button[j]['relief']='sunken'
        Plot['Pcontrol'][j] = 1
    refresh()

def add_plot(Name):
    global j, Plot
    Plot['Data'][j] = np.loadtxt(Name)    
    Plot['Pcontrol'].insert(j,1)
    Plot['Name'].insert(j,basename(Name)) 
    Plot['Plotbuttons'].insert(j, tk.Button(pltbutfrm, text=basename(Name), height=1,relief='sunken', command = lambda j=j : plot_select(basename(Name),Plot['Plotbuttons'])))
    #Plot['Plotbuttons'][j].grid( row = len(Plot['Plotbuttons']))
    Plot['Plotbuttons'][j].pack(fill='x')
    remove_menu.add_command(label=basename(Name),  command=lambda:remove_plot(basename(Name)))
    j = j + 1
    
    refresh()

def remove_plot(name_plot):
    global j, Plot, n

    i=Plot['Name'].index(name_plot);
    del  Plot['Data'][i] 
    Plot['Pcontrol'].pop(i)
    Plot['Name'].pop(i)
    Plot['Plotbuttons'][i].pack_forget() 
    Plot['Plotbuttons'].pop(i)
    remove_menu.delete(i)
    
       
    for x in range(j-i-1):
        Plot['Data'][x+i]= Plot['Data'][x+i+1]

    j=j-1
    n=n+1
    refresh()



########################################################################
########################################################################
# Menu

menubar = Tk.Menu(root, relief='raised', bd=2)
filemenu = Tk.Menu(menubar, tearoff=0)
filemenu.add_command(label="Open",command = lambda:add_plot(tkFileDialog.askopenfilename()))
filemenu.add_command(label ="Save")       #, command = lambda i=i: save('g12'))
filemenu.add_command(label ="Save As")    #, command = lambda i=i: saveas('g12'))
filemenu.add_command(label="Exit",command = lambda:_quit())        #, command = _quit)
menubar.add_cascade(label="File", menu=filemenu)
root.config(menu=menubar)

Interface['frames'].insert(0,tk.Frame(root,takefocus=0)) #Major frame
Interface['frames'][0].place(anchor='nw',relheight=1, relwidth=1)
Interface['frames'].insert(1,tk.Frame(Interface['frames'][0],takefocus=0)) #north west
Interface['frames'][1].place(anchor='nw',relheight=0.80, relwidth=0.80)
Interface['frames'].insert(2,tk.Frame(Interface['frames'][0],takefocus=0,relief='ridg',borderwidth=2)) #Bot  and left 
Interface['frames'][2].place(anchor='nw',relheight=0.20, relwidth=0.20,relx = 0.8,rely=0.8)
Interface['frames'].insert(3,tk.Frame(Interface['frames'][0],takefocus=0)) #top and right
Interface['frames'][3].place(anchor='nw',relheight=0.80, relwidth=0.20,relx = 0.8)
Interface['frames'].insert(4,tk.Frame(Interface['frames'][0],takefocus=0)) #bot and right
Interface['frames'][4].place(anchor='nw',relheight=0.20, relwidth=0.80,rely=0.8)

pltbutfrm =tk.Frame(Interface['frames'][3],takefocus=0)
Interface['frames'].insert(5,pltbutfrm) #frame que contem os botoes
Interface['frames'][5].pack(fill='x')



Interface['labels'].insert (0,tk.Label(Interface['frames'][2], text='Buffer'))
Interface['labels'][0].pack(fill='x')
Interface['labels'].insert (1,tk.Label(Interface['frames'][3], text='ROI'))
Interface['labels'][1].pack(fill='x')
Interface['labels'].insert (2,tk.Label(Interface['frames'][4], text='Test'))
Interface['labels'][2].pack(fill='x')


##############
#rig_but_menu#
##############
rig_but_menu= tk.Menu(root, tearoff=0)
rig_but_menu.add_command(label="Load")
rig_but_menu.add_command(label="Click/Unclick")


remove_menu = Tk.Menu(rig_but_menu, tearoff=0)

rig_but_menu.add_cascade(label="Remove plot", menu=remove_menu)


def mouse_TR(event):
    rig_but_menu.tk_popup(event.x_root, event.y_root)




# a tk.DrawingArea
canvas = FigureCanvasTkAgg(f, master=Interface['frames'][1])
canvas.get_tk_widget().place(anchor='nw',relheight=1, relwidth=1)
#canvas.show()
#canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

#toolbar = NavigationToolbar2TkAgg( canvas, root )
#toolbar.update()
#canvas._tkcanvas.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

Interface['frames'][3].bind("<ButtonRelease-3>", mouse_TR )#top_right



########################################################################
########################################################################
try:
    add_plot('Buffer.MCA')

except IOError:
   print tkMessageBox.askretrycancel("ERRO", "ERRO_10 ")
   _quit()
try:
    add_plot('Buffer.MCB')
except IOError:
   None
try:
    add_plot('Buffer.MCC')
except IOError:
   None






    #newfile = tkFileDialog.askopenfilename()
    #filefile = np.loadtxt(newfile)
    #tempfile = open('save.dat','w+')
    #set_fator(len(filefile))
    #for h in filefile[:,1]:
    #    tempfile.write(str(int(h))+'\n')
    #tempfile.close()
    



Tk.mainloop()
# If you put root.destroy() here, it will cause an error if
# the window is closed with the window manager.

